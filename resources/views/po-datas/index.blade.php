@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>Blue Conner Datas | Status : {{ $status or 'ALL' }}</h3></div>
                    <div class="card-body">
                        <a href="{{ url('/imports/podata') }}" class="btn btn-success btn-sm" title="Add New ShipData">
                            <i class="fa fa-upload" aria-hidden="true"></i> Upload Blue Conner Datas
                        </a>
                        <a href="{{ url('/file-uploads/create') }}" class="btn btn-success btn-sm" title="Add New ShipData">
                            <i class="fa fa-upload" aria-hidden="true"></i> Upload ใบขน
                        </a>
                        <a href="{{ url('/imports/shipdata') }}" class="btn btn-success btn-sm" title="Add New ShipData">
                            <i class="fa fa-upload" aria-hidden="true"></i> Upload รายการพร้อมเลขใบขน 
                        </a>
                        <a href="{{ url('/uploadtrans/create') }}" class="btn btn-success btn-sm" title="Add New ShipData">
                            <i class="fa fa-upload" aria-hidden="true"></i> Upload ใบโอนเงิน
                        </a>


                        <a href="{{ url('/imports/AllProcess') }}" class="btn btn-success btn-sm" title="Add New PoData">
                            <i class="fa fa-microchip" aria-hidden="true"></i> Match  ใบขน
                        </a>
                        <a href="{{ url('/imports/AllProcessCf') }}" class="btn btn-success btn-sm" title="Add New PoData">
                            <i class="fa fa-microchip" aria-hidden="true"></i> Match  C & F
                        </a>
                        <a href="{{ url('po-datas/export/filter') }}" class="btn btn-success btn-sm" title="Export">
                            <i class="fa fa-table" aria-hidden="true"></i> Export
                        </a>                        
                        <br/>                      
                        <br/>
                        <form method="GET" action="{{ url('/po-datas') }}" id="myForm" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="card-body">
                                <input type="text" class="form-control" name="search" placeholder="INV No..." value="{{ request('search') }}">
                                <input type="text" class="form-control" name="search2" placeholder="SO No..." value="{{ request('search2') }}">
                                
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>

                                &nbsp;&nbsp;
                                <a href="#">ปี</a>&nbsp;
                                <select id="dt_year" name="dt_year" class="form-select" onchange="myFunction('y', this.value);">
                                    <option value="">--ไม่ระบุ--</option>
                                    @foreach ($dt_po as $key=>$value)                        
                                        <option value="{{ $key }}" @if(request('dt_year')==$key) selected @endif>{{ $key }}</option>    
                                    @endforeach
                                </select>&nbsp;
                                <a href="#">เดือน</a>&nbsp;
                                {{-- @if(empty(request('dt_month'))) selected  @endif onchange="mySearch(this.value)"--}}
                                <select id="dt_month" name="dt_month" class="form-select month_search" onchange="mySearch(this.value)">
                                    {{-- <option value="">--ไม่ระบุ--</option>
                                    @if(!empty(request('dt_month')))
                                        @foreach ($dt_po[request('dt_year')] as $key=>$value)
                                            <option value="{{ $value }}" @if(request('dt_month')==$value) selected @endif>{{ $key }}-{{ $value }}</option> 
                                        @endforeach
                                    @endif --}}
                                </select>
                            </div>
                            <br/>
                        </form>

                        <a href="{{ url('/po-datas') }}" class="btn btn-primary btn-sm" title="ALL">
                            <i class="fa fa-globe" aria-hidden="true"></i> ALL
                        </a>
                        <a href="{{ url('/po-datas?status=Process') }}" class="btn btn-primary btn-sm" title="Status Process">
                            <i class="fa fa-play" aria-hidden="true"></i> Process
                        </a>
                        <a href="{{ url('/po-datas?status=Complete') }}" class="btn btn-primary btn-sm" title="Status Complete">
                            <i class="fa fa-check-square" aria-hidden="true"></i> Complete
                        </a>
                        <a href="{{ url('/po-datas?status=reject') }}" class="btn btn-primary btn-sm" title="Status Reject">
                            <i class="fa fa-chain-broken" aria-hidden="true"></i> Reject
                        </a>
                        <br/>
                        {{-- @foreach ($monthdata as $monthitem)                        
                            [<a href="{{ url('/po-datas?monthsearch='.$monthitem->monthlist) }}" title="Status Reject">
                                {{ $monthitem->monthlist }}
                            </a>]    
                        @endforeach --}}
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>CSN / Order Name / Sale Order / Invoice</th>
                                        <th>Loading Date<br/>Follow up date</th>
                                        <th>Match no.<br>/จำนวนสินค้า</th>
                                        <th>Tax Return</th>
                                        <th>Link</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($podatas as $item)
                                    <tr>
                                        <td 
                                            @if ( $item->main_status == 'reject')
                                                style="background-color:red;color:white;"
                                            @endif
                                            >{{ $loop->iteration }}</td>
                                        <td 
                                            @if ( $item->main_status == 'reject')
                                                style="background-color:red;color:white;"
                                            @endif
                                            ><a href="{{ url('/po-datas/' . $item->id) }}" title="View PoData">{{ $item->CSN }} / {{ $item->order_name }} <br/>/ {{ $item->sale_order_name }} / {{ $item->inv_name }}</a></td>
                                        @if ( strtotime('+14 day', strtotime($item->loading_date)) <= strtotime(date('Y-m-d')))
                                          <td style="background-color:red;color:white;text-align:center;">{{ $item->loading_date }}<br/>{{ date('Y-m-d', strtotime('+14 day', strtotime($item->loading_date))) }}</td>
                                          
                                        @else
                                          <td style="text-align:center;">{{ $item->loading_date }}<br/>{{ date('Y-m-d', strtotime('+14 day', strtotime($item->loading_date))) }}</td>
                                          
                                        @endif
                                        <td style="text-align:center;
                                            @if ( $item->main_status == 'reject')
                                                background-color:red;color:white;
                                            @endif
                                            "
                                            >
                                        @php
                                            $chknotmatch = 0;
                                            foreach($item->podatadetails as $subitem){
                                                if(isset($subitem->shipdata->BHT)){
                                                    $chknotmatch++;
                                                }
                                            }
                                        @endphp
                                          {{ $chknotmatch }}  /{{ $item->podatadetails->count() }}</td>
                                        <td 
                                            @if ( $item->main_status == 'reject')
                                                style="background-color:red;color:white;"
                                            @endif
                                            >
                                            @php
                                              //  if($item->status == 'Match  Trans / C & F'){
                                                    $totaltax = 0;
                                                    foreach ($item->podatadetails as $itemdetail) {
                                                        if(isset($itemdetail->shipdata->BHT) && !empty($itemdetail->ship_data_id)){
                                                            $totaltax += ($itemdetail->shipdata->BHT * $itemdetail->tax_rate)/100;
                                                        }
                                                    }
                                                    echo number_format($totaltax,2,".",",");
                                             //   }
                                            @endphp
                                        </td>
                                        
                                        <td 
                                            @if ( $item->main_status == 'reject')
                                                style="background-color:red;color:white;"
                                            @endif
                                            >
                                            @if ($item->status_trans == 'Yes')
                                                <button class="btn btn-success btn-sm" title="Match  ใบขนแล้ว" ><i class="fa fa-truck" aria-hidden="true"></i></button>
                                            @else
                                                <button class="btn btn-light btn-sm" title="ยังไม่ได้ Match  ใบขน"><i class="fa fa-truck" aria-hidden="true"></i></button>
                                            @endif
                                            @if ($item->status_cnf == 'Yes')
                                                <button class="btn btn-success btn-sm" title="Match  C & F แล้ว" ><i class="fa fa-usd" aria-hidden="true"></i></button>
                                            @else
                                                <button class="btn btn-light btn-sm" title="ยังไม่ได้ Match  C & F" ><i class="fa fa-usd" aria-hidden="true"></i></button>
                                            @endif
                                               @php
                                                   $flagcanclosed = true;
                                               @endphp
                                               @if ($item->fileupload->count() > 0)
                                                  <a href="{{ url($item->fileupload[0]->serverpath) }}" title="Upload ใบขนแล้ว"><button class="btn btn-success btn-sm"><i class="fa fa-upload" aria-hidden="true"></i></button></a>
                                               @else
                                                   <a href="{{ url('/file-uploads/create') }}" title="ยังไม่ได้ Upload ใบขน"><button class="btn btn-light btn-sm" ><i class="fa fa-upload" aria-hidden="true"></i></button></a>
                                                    @php
                                                   $flagcanclosed = false;
                                               @endphp
                                               @endif
                                               @if ($item->print_status == '')
                                               @php
                                                   $flagcanclosed = false;
                                               @endphp
                                               <a href="{{ url('/po-datas/changestatus/' . $item->id) }}" title="ยังไม่ได้ Print เอกสาร"><button class="btn btn-light btn-sm"><i class="fa fa-print" aria-hidden="true"></i></button></a>
                                               @else
                                               <a href="{{ url('/po-datas/changestatus/' . $item->id) }}" title="Print เอกสารแล้ว"><button class="btn btn-success btn-sm"><i class="fa fa-print" aria-hidden="true"></i></button></a>
                                               @endif

                                               @if ($item->banktransd->count() > 0)
                                                   @php
                                                       $totalusd = 0;
                                                   @endphp
                                                    @foreach ($item->banktransd()->get() as $item2)
                                                    @php
                                                        $totalusd += $item2->income_usd;
                                                    @endphp
                                                   @endforeach
                                                   @foreach ($item->banktransd()->get() as $item2)
                                                        @if (($item->candf - $totalusd) > 0 )
                                                            <a href="{{ url('/uploadtrans/view/' . $item2->bank_trans_m_id) }}" title="โอนเงิน ยังไม่ครบ"><button class="btn btn-light btn-sm"><i class="fa fa-money" aria-hidden="true"></i></button></a>
                                                        @else
                                                            <a href="{{ url('/uploadtrans/view/' . $item2->bank_trans_m_id) }}" title="โอนเงิน ครบแล้ว"><button class="btn btn-success btn-sm"><i class="fa fa-money" aria-hidden="true"></i></button></a>
                                                        @endif
                                                        @if (!empty($item2->banktransm->processpath))
                                                            <a href="{{ url('/uploadtrans/genpdf/'.$item2->bank_trans_m_id) }}" title="พิมพ์ใบโอนเงิน"><button class="btn btn-success btn-sm"><i class="fa fa-credit-card" aria-hidden="true"></i></button></a>
                                                        @else
                                                            @if (!empty($item2->banktransm->serverpath))
                                                                <a href="{{ url('/'.$item2->banktransm->serverpath) }}" title="พิมพ์ใบโอนเงิน"><button class="btn btn-success btn-sm"><i class="fa fa-credit-card" aria-hidden="true"></i></button></a>
                                                            @endif
                                                        @endif
                                                   
                                                   @endforeach
                                               @else
                                                   
                                               @endif
                                               
                                        </td>
                                        <td 
                                            @if ( $item->main_status == 'reject')
                                                style="background-color:red;color:white;"
                                            @endif
                                            >
                                            @if ($flagcanclosed && $item->main_status != 'Complete')
                                                <a href="{{ url('/po-datas/changemainstatus/' . $item->id .'/Complete') }}" title="View PoData"><button class="btn btn-primary btn-sm"><i class="fa fa-check-circle" aria-hidden="true"></i></button></a>
                                            @else
                                               {{ $item->main_status }} 
                                            @endif
                                            
                                        </td>
                                       <td 
                                            @if ( $item->main_status == 'reject')
                                                style="background-color:red;color:white;"
                                            @endif
                                            >
                                            
                                            
                                            <form method="POST" action="{{ url('/po-datas' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete PoData" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                            </form>
                                            @if ($item->main_status == 'reject')
                                                <a href="{{ url('/po-datas/changemainstatus/' . $item->id .'/Process') }}" title="View PoData"><button class="btn btn-primary btn-sm"><i class="fa fa-link" aria-hidden="true"></i></button></a>
                                            @else
                                                <a href="{{ url('/po-datas/changemainstatus/' . $item->id .'/reject') }}" title="View PoData"><button class="btn btn-primary btn-sm"><i class="fa fa-chain-broken" aria-hidden="true"></i></button></a>
                                            @endif

                                             
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                             <div class="pagination-wrapper"> 
                                {!! $podatas->appends([
                                    'search' => Request::get('search'),
                                    'search2' => Request::get('search2'),
                                    'dt_year' => Request::get('dt_year'),
                                    'dt_month' => Request::get('dt_month'),
                                    'status' => Request::get('status')
                                ])->render() !!} </div>
                             {{-- <div class="pagination-wrapper"> {!! $podatas->appends(['search' => Request::get('search'),'search2' => Request::get('search2'),'monthsearch' => Request::get('monthsearch'),'status' => Request::get('status')])->render() !!} </div> --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var url_string = window.location.href;
        var url = new URL(url_string);
        var param_month = url.searchParams.get("dt_month");        
        var param_year = url.searchParams.get("dt_year");
        // var dt_year = document.getElementById("dt_year");
        // var dt_month = document.getElementById("dt_month");
        console.log('m->'+param_month+' y->'+param_year);
        if(param_month){
            Window.onload = myFunction('m', param_month);
        }else if(param_year){
            Window.onload = myFunction('y', param_year);
        }

        function myFunction(type, i) {  
            console.log(type+', '+i);
            $("#dt_month").empty();  
            if(type==='m'){
                var url_string = window.location.href;
                var url = new URL(url_string);
                var param_year = url.searchParams.get("dt_year");
                var param_month = i;
            }else{
                var param_year = i;
            }
            

            var dt_month = document.getElementById("dt_month");
            
            if(param_year){
                var obj = <?php echo json_encode($dt_po); ?>;
                // console.log(obj[i]);
                if(obj[param_year]){   
                    // var option = document.createElement("option");
                    $("<option/>", {
                        value: '',
                        text: '--ไม่ระบุ--'
                    }).appendTo(dt_month);

                    var opt = document.createElement('option');
                    $.each(obj[param_year], function(index, value){
                        console.log(index+'->'+value);
                        $("<option/>", {
                            value: value,
                            text: index
                        }).appendTo(dt_month);     
                    });  
                    
                    // if(param_month){
                    //     console.log('param_month->'+param_month);
                    //     dt_month.value = param_month;
                    // } 
                    $("#dt_month [value='"+param_month+"']").attr("selected","selected");                              
                }
            }else{
                $("<option/>", {
                    value: '',
                    text: '--ไม่ระบุ--'
                }).appendTo(dt_month);
            }
        }

        function mySearch(i) {
            console.log(i);
            if(i){
                // var link_url = window.location.href+"?dt_month="+i;
                // console.log(window.location);
                // location.reload();
                // window.location.reload();
                document.getElementById("myForm").submit();
            }
        }
    </script>
@endsection
